# trpcd

![](https://img.shields.io/badge/written%20in-Golang-blue)

A custom server implementing the Transmission RPC protocol.

Trpcd is a server implementation of the Transmission RPC protocol, allowing you to implement custom services that are compatible with Transmission's web, desktop, and third-party clients.

This project lays groundwork that may be useful for other projects. Some ideas are
- Transmission service multiplexing
- Torrent isolation on a single transmission service process
- Using transmission's web/desktop interface for non-Transmission products e.g. `aria2c` or `youtube-dl`
- Adding web and youtube-dl support to Transmission's web and desktop clients
- Supporting arbitrary line I/O via an obscure system

Implementations must provide `trpcd` with a virtual torrent system via a range of implementation functions. The project then produces an `http.ServeMux` that can either be passed to `http.ListenAndServe` directly, or muxed further with other services in your final binary.

Tags: torrent

## License

This project includes a copy of Transmission's own web interface, which is licensed under the GPL (see the `webroot/LICENSE` file for more details). Any binaries built with this functionality included must be licensed under the GPL license.

It is possible to remove the web functionality without losing compatibility with Transmission's desktop clients. In this case, the remaining files that were written by the `trpcd` project are licensed more permissively.

## Changelog

2017-01-29 0.14
- Initial public release
- [⬇️ trcpd-src-0.14.tar.xz](dist-archive/trcpd-src-0.14.tar.xz) *(574.50 KiB)*

